from django.urls import path
from . import views

app_name = "home"
#url for app
urlpatterns = [
    path('', views.homePage, name= 'homePage'),
    path('portfolio/', views.portfolio, name= 'portfolio')
]
