from django.urls import path
from . import views

app_name = "jadwals"
#url for app
urlpatterns = [
    path('', views.jadwal_create, name='create'),
    path('<id>/', views.jadwal_delete, name='delete')
]