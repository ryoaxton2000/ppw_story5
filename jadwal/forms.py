from django import forms
from . import models

class InputDate(forms.DateInput):
    input_type = 'date'

class InputTime(forms.TimeInput):
    input_type = 'time'
        
class createJadwal(forms.ModelForm):
    class Meta:
        model = models.Jadwal
        fields = ['nama_kegiatan', 'tempat_kegiatan', 'kategori_kegiatan', 'tanggal_kegiatan', 'jam_kegiatan']
        widgets = {'tanggal_kegiatan' : InputDate,
                   'jam_kegiatan' : InputTime}