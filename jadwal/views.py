from django.shortcuts import render, get_object_or_404, redirect
from . import forms
from . models import Jadwal
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.

def jadwal_create(request):
    if request.method == 'POST':
        form = forms.createJadwal(request.POST)
        if form.is_valid() :
            objectJadwal = form.save()
            objectJadwal.hari_kegiatan = objectJadwal.tanggal_kegiatan.strftime('%A')     
            objectJadwal.save()
            return redirect('jadwals:create')
    else :
        form = forms.createJadwal()
    jadwal = Jadwal.objects.all().order_by('tanggal_kegiatan', 'jam_kegiatan')
    return render(request, 'jadwal.html', {'form':form, 'jadwal':jadwal})

def jadwal_delete(request, id):
    jdwl = Jadwal.objects.get(id=id)
    jdwl.delete()
    return redirect('jadwals:create')