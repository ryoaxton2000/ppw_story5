from django.db import models
import datetime

# Create your models here.

class Jadwal(models.Model):
    nama_kegiatan = models.CharField(max_length = 50)
    hari_kegiatan = models.CharField(default = "", max_length = 10)
    tanggal_kegiatan = models.DateField()
    jam_kegiatan = models.TimeField(default="", null=True)
    tempat_kegiatan = models.CharField(max_length = 70)
    kategori_kegiatan =  models.CharField(max_length = 20)